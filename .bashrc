[ -f ~/.bash_exports ] && . ~/.bash_exports
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

# When fish is installed,
# it isn't the parent process and
# shell is running inside pseudo terminal
# use fish instead of bash
if [ -f /usr/bin/fish ] && \
   [ "$(cat /proc/$PPID/comm)" != "fish" ] && \
   [[ "$(tty)" == "/dev/pts/"* ]]; then
  exec fish
fi


# Load Angular CLI autocompletion.
source <(ng completion script)
