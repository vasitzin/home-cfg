if mp.get_property_bool 'osc' then
  for _, loader in pairs(package.loaders) do
    res = loader '@osc.lua'
    if type(res) == 'function' then
      res '@osc.lua'
      break
    end
  end

  mp.register_event('start-file', show_osc)
  mp.register_event('file-loaded', show_osc)
  mp.register_event('seek', show_osc)
end
