local M = {}

M.setup = function()
  local fn = require('winshift').cmd_winshift
  vim.keymap.set('n', '<C-s>', function() fn 'swap' end)
  vim.keymap.set('n', '<C-M-h>', function() fn 'left' end)
  vim.keymap.set('n', '<C-M-j>', function() fn 'down' end)
  vim.keymap.set('n', '<C-M-k>', function() fn 'up' end)
  vim.keymap.set('n', '<C-M-l>', function() fn 'right' end)
end

return M
