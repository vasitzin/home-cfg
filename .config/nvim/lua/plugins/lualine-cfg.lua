local M = {}

M.deps = {
  'nvim-tree/nvim-web-devicons',
  'meuter/lualine-so-fancy.nvim',
  'SmiteshP/nvim-navic',
}

M.setup = function()
  local navic = require 'nvim-navic'
  require('lualine').setup({
    options = {
      always_divide_middle = true,
      icons_enabled = true,
      globalstatus = true,
      component_separators = { left = '', right = '' },
      section_separators = { left = '', right = '' },
    },
    sections = {
      lualine_a = {
        {
          'tabs',
          symbols = {
            modified = '+',
          },
        },
      },
      lualine_b = {},
      lualine_c = {},
      lualine_x = {
        'diff',
        'branch',
        'fancy_lsp_servers',
        {
          'fileformat',
          symbols = {
            unix = ' unix',
            dos = ' dos',
            mac = ' mac',
          },
        },
        'encoding',
        'location',
      },
      lualine_y = {},
      lualine_z = {},
    },
    winbar = {
      lualine_a = {
        {
          'filetype',
          icon_only = true,
          colored = false,
        },
        {
          'filename',
          symbols = {
            modified = '●',
            readonly = '[RO]',
          },
        },
      },
      lualine_c = { function() return navic.get_location() end },
      lualine_x = {
        {
          'diagnostics',
          draw_empty = true,
        },
      },
    },
    inactive_winbar = {
      lualine_c = {
        {
          'filetype',
          icon_only = true,
          colored = false,
        },
        {
          'filename',
          symbols = {
            modified = '●',
            readonly = '[RO]',
          },
        },
        { function() return navic.get_location() end },
      },
      lualine_x = {
        {
          'diagnostics',
          draw_empty = true,
        },
      },
    },
  })
end

return M
