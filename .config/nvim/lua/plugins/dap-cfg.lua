local M = {}

-- return vim.fn.glob(x:get_install_path() .. '/abc-*.xyz')

function Programfn() return vim.fn.input('Path to executable: ', vim.fn.getcwd() .. '/', 'file') end

function Argsfn() return vim.split(vim.fn.input 'Arguments (optional): ', ' ') end

M.deps = { 'mfussenegger/nvim-dap', 'nvim-neotest/nvim-nio' }

-- More info at: https://github.com/mfussenegger/nvim-dap/wiki/Debug-Adapter-installation
M.setup = function(opts)
  local dap, dui, reg = require 'dap', require 'dapui', require 'mason-registry'
  local adapter, pack, path

  dui.setup()

  adapter = 'gdb'
  pack = nil
  path = nil
  dap.adapters[adapter] = {
    args = { '--interpreter=dap', '--annotate=3' },
    command = adapter,
    name = adapter,
    type = 'executable',
  }
  dap.configurations.c = {
    {
      args = Argsfn,
      cwd = '${workspaceFolder}',
      name = 'Launch file',
      program = Programfn,
      request = 'launch',
      stopAtBeginningOfMainSubprogram = false,
      type = adapter,
    },
  }
  dap.configurations.cpp = dap.configurations.c
  dap.configurations.rust = dap.configurations.c
  dap.configurations.zig = dap.configurations.c

  adapter = 'bashdb'
  pack = reg.get_package 'bash-debug-adapter'
  path = nil
  dap.adapters[adapter] = {
    command = vim.fn.stdpath 'data' .. '/mason/packages/bash-debug-adapter/bash-debug-adapter',
    name = adapter,
    type = 'executable',
  }
  dap.configurations.sh = {
    {
      args = Argsfn,
      cwd = '${workspaceFolder}',
      env = {},
      file = '${file}',
      name = 'Launch file',
      pathBash = '/bin/bash',
      pathBashdb = pack:get_install_path() .. '/extension/bashdb_dir/bashdb',
      pathBashdbLib = pack:get_install_path() .. '/extension/bashdb_dir',
      pathCat = 'cat',
      pathMkfifo = 'mkfifo',
      pathPkill = 'pkill',
      program = '${file}',
      request = 'launch',
      showDebugOutput = true,
      terminalKind = 'integrated',
      trace = true,
      type = adapter,
    },
  }

  adapter = 'pwa-node'
  pack = reg.get_package 'js-debug-adapter'
  path = pack:get_install_path() .. '/js-debug/src/dapDebugServer.js'
  dap.adapters[adapter] = {
    executable = { command = 'node', args = { path, '${port}' } },
    host = 'localhost',
    name = adapter,
    port = '${port}',
    type = 'server',
  }
  dap.configurations.javascript = {
    {
      args = Argsfn,
      cwd = '${workspaceFolder}',
      name = 'Launch file',
      program = Programfn,
      request = 'launch',
      type = adapter,
    },
  }

  adapter = 'python'
  pack = reg.get_package 'debugpy'
  path = pack:get_install_path() .. '/venv/bin/python'
  dap.adapters[adapter] = {
    args = { '-m', 'debugpy.adapter' },
    command = path,
    name = adapter,
    options = { source_filetype = 'python' },
    type = 'executable',
  }
  dap.configurations.python = {
    {
      args = Argsfn,
      cwd = '${workspaceFolder}',
      name = 'Launch file',
      program = Programfn,
      pythonPath = '/usr/bin/python',
      request = 'launch',
      type = adapter,
    },
  }

  dap.listeners.after.event_initialized['dapui_config'] = function() dui.open() end

  vim.keymap.set('n', '<Leader>bb', function() dap.toggle_breakpoint() end, opts)
  vim.keymap.set('n', '<Leader>br', function() dap.clear_breakpoints() end, opts)
  vim.keymap.set('n', '<Leader>dc', function() dap.continue() end, opts)
  vim.keymap.set('n', '<Leader>dj', function() dap.step_over() end, opts)
  vim.keymap.set('n', '<Leader>dk', function() dap.step_into() end, opts)
  vim.keymap.set('n', '<Leader>do', function() dap.step_out() end, opts)
  vim.keymap.set('n', '<Leader>dx', function()
    dap.terminate()
    dui.close()
  end, opts)
end

return M
