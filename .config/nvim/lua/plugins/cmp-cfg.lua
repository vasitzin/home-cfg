local M = {}

M.deps = {
  'hrsh7th/vim-vsnip',
  'hrsh7th/cmp-vsnip',
  'hrsh7th/cmp-nvim-lsp',
  'hrsh7th/cmp-buffer',
  'hrsh7th/cmp-path',
  'neovim/nvim-lspconfig',
}

M.setup = function()
  local cmp = require 'cmp'
  cmp.setup({
    snippet = {
      expand = function(args) vim.fn['vsnip#anonymous'](args.body) end,
    },
    sources = cmp.config.sources({
      { name = 'vsnip' },
      { name = 'nvim_lsp' },
      { name = 'buffer' },
      { name = 'path' },
    }),
    mapping = cmp.mapping.preset.insert({
      ['<Up>'] = cmp.config.disable,
      ['<Left>'] = cmp.config.disable,
      ['<Down>'] = cmp.config.disable,
      ['<Right>'] = cmp.config.disable,
      ['<C-Escape>'] = cmp.mapping.abort(),
      ['<C-e>'] = cmp.mapping.complete(),
      ['<C-b>'] = cmp.mapping.scroll_docs(-3),
      ['<C-f>'] = cmp.mapping.scroll_docs(3),
      ['<C-Up>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Select }),
      ['<C-Down>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Select }),
      ['<C-CR>'] = cmp.mapping.confirm({ select = true }),
    }),
  })
end

return M
