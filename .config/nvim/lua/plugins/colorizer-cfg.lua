local M = {}

M.setup = function() require('colorizer').setup({ filetypes = { '*' } }) end

return M
