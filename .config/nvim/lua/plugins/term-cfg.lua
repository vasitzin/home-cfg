local M = {}

M.setup = function(opts)
  require('toggleterm').setup({})
  vim.keymap.set('n', '<F5>', ':ToggleTerm direction=float<CR>', opts)
  vim.keymap.set('t', '<F5>', '<C-\\><C-n>:ToggleTerm direction=float<CR>', opts)
end

return M
