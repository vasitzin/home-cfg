local M = {}

M.deps = {
  'nvim-lua/plenary.nvim',
  'nvim-tree/nvim-web-devicons',
  'MunifTanjim/nui.nvim',
}

M.setup = function(gopts)
  require('neo-tree').setup({
    window = {
      mappings = {
        ['Z'] = 'expand_all_nodes',
        ['C'] = 'close_all_subnodes',
      },
    },
  })
  vim.keymap.set('n', 'F', ':Neotree toggle float<CR>', gopts)
end

return M
