local M = {}

M.setup = function(opts) vim.keymap.set('n', '<Leader>bd', ':BDelete hidden<CR>', opts) end

return M
