local M = {}

M.setup = function(opts)
  require('maximizer').setup({})
  vim.keymap.set('n', 'M', function() require('maximizer').toggle() end, opts)
end

return M
