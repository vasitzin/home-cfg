local M = {}

M.setup = function()
  require('nvim-treesitter.configs').setup({
    highlight = {
      enable = true,
      additional_vim_regex_highlighting = false,
      disable = { 'dockerfile' },
    },
    indent = {
      enable = false,
      disable = {},
    },
    modules = {},
    ensure_installed = 'all',
    sync_install = false,
    auto_install = true,
    ignore_install = {},
  })
end

return M
