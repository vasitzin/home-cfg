local M = {}

M.setup = function(opts)
  vim.g.neoformat_enabled_go = { 'gofmt' }
  vim.keymap.set('n', '<Leader>f', ':Neoformat<CR><Esc>', opts) -- format text
end

return M
