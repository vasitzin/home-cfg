local M = {}

M.keys = {
  { '<leader>m', ':Mason<CR>', mode = 'n' },
}

return M
