local M = {}

local ingore_these = {
  '.git/',
  'node_modules/',
  '__pycache__',
  '%.a',
  '%.o',
  '%.class',
  '%.jar',
  'build/',
  'target/',
}

local cbff = { fuzzy = false }

M.deps = {
  'nvim-lua/plenary.nvim',
  'nvim-tree/nvim-web-devicons',
  {
    'nvim-telescope/telescope-fzf-native.nvim',
    build = 'cmake -S. -Bbuild -DCMAKE_BUILD_TYPE=Release && cmake --build build --config Release && cmake --install build --prefix build',
  },
  'nvim-telescope/telescope-dap.nvim',
}

M.setup = function(opts)
  local actions = require 'telescope.actions'
  local builtins = require 'telescope.builtin'
  local fzf = require('telescope').load_extension 'fzf'
  local dap = require('telescope').load_extension 'dap'
  local ivy = require('telescope.themes').get_ivy()
  require('telescope').setup({
    defaults = {
      file_ignore_patterns = ingore_these,
      sorting_strategy = 'ascending',
      mappings = {
        i = {
          ['<CR>'] = actions.select_tab_drop,
          ['<C-CR>'] = actions.select_default,
        },
      },
    },
    pickers = {},
  })
  vim.keymap.set('n', '<Leader><Leader>', function() builtins.buffers(ivy) end, opts)
  vim.keymap.set('n', '<Leader>#', function() builtins.current_buffer_fuzzy_find(cbff) end, opts)
  vim.keymap.set('n', '<F6>', function() builtins.find_files(ivy) end, opts)
  vim.keymap.set('n', '<Leader><F6>', function() builtins.live_grep(ivy) end, opts)
  vim.keymap.set('n', '<F7>', function() builtins.quickfix(ivy) end, opts)
  vim.keymap.set('n', '<Leader><F7>', function() builtins.diagnostics(ivy) end, opts)
  vim.keymap.set('n', '<F8>', function() builtins.jumplist(ivy) end, opts)
  vim.keymap.set('n', '<Leader><F8>', function() builtins.loclist(ivy) end, opts)
  vim.keymap.set('n', '<Leader>bl', function() dap.list_breakpoints(ivy) end, opts)
end

return M
