local M = {}

-- More info at: https://github.com/neovim/nvim-lspconfig/blob/master/doc/configs.md
M.setup = function(lsp_on_attach)
  local lsp = require 'lspconfig'
  local default_obj = {
    on_attach = lsp_on_attach,
    capabilities = require('cmp_nvim_lsp').default_capabilities(),
  }
  local lua_obj = {
    unpack(default_obj),
    settings = {
      Lua = {
        runtime = { version = 'LuaJIT' },
        diagnostics = { globals = { 'vim' } },
        workspace = {
          checkThirdParty = false,
          library = vim.api.nvim_get_runtime_file('', true),
        },
        telemetry = { enable = false },
      },
    },
  }
  local omnisharp_obj = {
    unpack(default_obj),
    cmd = {
      'dotnet',
      '/home/vasilis/.local/share/nvim/mason/packages/omnisharp/libexec/OmniSharp.dll',
    },
  }
  lsp.angularls.setup(default_obj)
  lsp.bashls.setup(default_obj)
  lsp.clangd.setup(default_obj)
  lsp.cmake.setup(default_obj)
  lsp.cssls.setup(default_obj)
  lsp.eslint.setup(default_obj)
  lsp.gopls.setup(default_obj)
  lsp.html.setup(default_obj)
  lsp.jdtls.setup(default_obj)
  lsp.jsonls.setup(default_obj)
  lsp.lua_ls.setup(lua_obj)
  lsp.omnisharp.setup(omnisharp_obj)
  lsp.pyright.setup(default_obj)
  lsp.rust_analyzer.setup(default_obj)
  lsp.somesass_ls.setup(default_obj)
  lsp.sqls.setup(default_obj)
  lsp.svelte.setup(default_obj)
  lsp.tailwindcss.setup(default_obj)
  lsp.texlab.setup(default_obj)
  lsp.tinymist.setup(default_obj)
  lsp.ts_ls.setup(default_obj)
  lsp.vuels.setup(default_obj)
  lsp.yamlls.setup(default_obj)
  lsp.zls.setup(default_obj)
end

return M
