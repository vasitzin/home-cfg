local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    'git',
    'clone',
    '--filter=blob:none',
    'https://github.com/folke/lazy.nvim.git',
    '--branch=stable', -- latest stable release
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

-- local border = { { '╭' }, { '─' }, { '╮' }, { '│' }, { '╯' }, { '─' }, { '╰' }, { '│' } }
local opts = { noremap = true, silent = true }
local lsp_on_attach = function(client, bufnr)
  local bufopts = { noremap = true, silent = true, buffer = bufnr }
  local w_bufopts = { noremap = true, silent = false, buffer = bufnr }
  local list_workspace_folders = function() vim.inspect(vim.lsp.buf.list_workspace_folders()) end
  if client.supports_method 'textDocument/completion' then
    vim.bo[bufnr].omnifunc = 'v:lua.vim.lsp.omnifunc'
  end
  if client.supports_method 'textDocument/definition' then
    vim.bo[bufnr].tagfunc = 'v:lua.vim.lsp.tagfunc'
  end
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<Leader>k', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<Leader>D', vim.lsp.buf.type_definition, w_bufopts)
  vim.keymap.set('n', '<Leader>R', vim.lsp.buf.rename, w_bufopts)
  vim.keymap.set('n', '<Leader>wa', vim.lsp.buf.add_workspace_folder, w_bufopts)
  vim.keymap.set('n', '<Leader>wr', vim.lsp.buf.remove_workspace_folder, w_bufopts)
  vim.keymap.set('n', '<Leader>wl', vim.lsp.buf.list_workspace_folders, w_bufopts)
  vim.keymap.set('n', '<Leader>ca', vim.lsp.buf.code_action, w_bufopts)
  vim.keymap.set('n', '<Leader>e', vim.diagnostic.open_float, opts)
  vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
  vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
  vim.keymap.set('n', '<Leader>q', vim.diagnostic.setloclist, opts)
  if client.server_capabilities.documentSymbolProvider then
    local navic = require 'nvim-navic'
    navic.setup(require('plugins.navic-cfg').opts)
    navic.attach(client, bufnr)
  end
end

require('lazy').setup({
  {
    'rebelot/kanagawa.nvim',
    priority = 990,
    config = function() vim.cmd 'colorscheme kanagawa-wave' end,
  },
  {
    'levouh/tint.nvim',
    priority = 980,
    config = true,
  },
  {
    'nvim-lualine/lualine.nvim',
    priority = 970,
    dependencies = require('plugins.lualine-cfg').deps,
    config = function() require('plugins.lualine-cfg').setup() end,
  },
  {
    'lukas-reineke/indent-blankline.nvim',
    priority = 960,
    main = 'ibl',
    config = true,
  },
  {
    'nvim-treesitter/nvim-treesitter',
    priority = 890,
    config = function() require('plugins.treesitter-cfg').setup() end,
  },
  {
    'neovim/nvim-lspconfig',
    priority = 870,
    config = function() require('plugins.lspconfig-cfg').setup(lsp_on_attach) end,
  },
  {
    'nvim-telescope/telescope.nvim', -- install ripgrep on system
    priority = 860,
    dependencies = require('plugins.telescope-cfg').deps,
    config = function() require('plugins.telescope-cfg').setup(opts) end,
  },
  {
    'rcarriga/nvim-dap-ui',
    priority = 850,
    dependencies = require('plugins.dap-cfg').deps,
    config = function() require('plugins.dap-cfg').setup(opts) end,
  },
  {
    'kazhala/close-buffers.nvim',
    config = function() require('plugins.bufdel-cfg').setup(opts) end,
  },
  {
    'numToStr/Comment.nvim',
    config = true,
  },
  {
    'kylechui/nvim-surround',
    config = true,
  },
  {
    'sbdchd/neoformat',
    config = function() require('plugins.format-cfg').setup(opts) end,
  },
  {
    'hrsh7th/nvim-cmp',
    dependencies = require('plugins.cmp-cfg').deps,
    event = { 'InsertEnter' },
    config = function() require('plugins.cmp-cfg').setup() end,
  },
  {
    'sindrets/winshift.nvim',
    config = function() require('plugins.winshift-cfg').setup() end,
  },
  {
    '0x00-ketsu/maximizer.nvim',
    config = function() require('plugins.max-cfg').setup(opts) end,
  },
  {
    'NvChad/nvim-colorizer.lua',
    config = function() require('plugins.colorizer-cfg').setup() end,
  },
  {
    'nvim-neo-tree/neo-tree.nvim',
    dependencies = require('plugins.tree-cfg').deps,
    config = function() require('plugins.tree-cfg').setup(opts) end,
  },
  {
    'akinsho/toggleterm.nvim',
    config = function() require('plugins.term-cfg').setup(opts) end,
  },
  {
    'williamboman/mason.nvim',
    keys = require('plugins.mason-cfg').keys,
    config = true,
  },
  {
    'Grafcube/suedit.nvim',
    dependencies = 'akinsho/toggleterm.nvim',
    opts = { cmd = 'doas' }, -- sudo run0
  },
}, {
  checker = { enabled = false },
  debug = false,
})
