local TERM = '!alacritty &'
local TERMHERE = '!alacritty --working-directory "%:h" &'
local FILE = '!dolphin . &'
local FILEHERE = '!dolphin %:h &'
local opts = { noremap = true, silent = true }
local noremap = { noremap = true }

local function back(pos)
  local str = ''
  while pos > 0 do
    str = str .. '<Left>'
    pos = pos - 1
  end
  return str
end

-- FILESYSTEM
vim.keymap.set('n', '<F1>', ':w<CR>', opts) -- save current
vim.keymap.set('n', '!<F1>', ':w!<CR>', opts) -- save current
vim.keymap.set('n', '<Leader><F1>', ':wa<CR>', opts) -- save all
vim.keymap.set('n', '<Leader>!<F1>', ':wa!<CR>', opts) -- save all
vim.keymap.set('n', '<F2>', ':mks! Session.vim<CR>', opts) -- save session
vim.keymap.set('n', '<Leader><F2>', ':source Session.vim<CR>', opts) -- restore session
vim.keymap.set('n', '<F3>', ':enew<CR>', opts) -- hide buffer keep win
vim.keymap.set('n', '<Leader><F3>', ':enew|bd#<CR>', opts) -- delete buffer keep win
vim.keymap.set('n', '<F4>', ':bd<CR>', opts) -- delete buffer close win
vim.keymap.set('n', '<Leader><F4>', ':qa<CR>', opts) -- quit editor

-- TABS
vim.keymap.set('n', '<F9>', ':tabnew<CR>', opts) -- new tab
vim.keymap.set('n', '<F10>', ':tabprevious<CR>', opts) -- previous tab
vim.keymap.set('n', '<Leader><F10>', ':tabmove -1<CR>', opts) -- move tab left
vim.keymap.set('n', '<F11>', ':tabnext<CR>', opts) -- next tab
vim.keymap.set('n', '<Leader><F11>', ':tabmove +1<CR>', opts) -- move tab right
vim.keymap.set('n', '<F12>', ':tabclose<CR>', opts) -- close tab

-- WINDOWS
vim.keymap.set('n', '<Leader>h', ':sp|enew<CR>', opts) -- split horizontal
vim.keymap.set('n', '<Leader>v', ':vs|enew<CR>', opts) -- split vertical
vim.keymap.set('n', '<Tab>', ':wincmd w<CR>', opts) -- go to next
vim.keymap.set('n', '<S-Tab>', ':wincmd W<CR>', opts) -- go to previous
vim.keymap.set('n', '<M-0>', ':wincmd =<CR>', opts) -- normalize panel sizes
vim.keymap.set('n', '<M-Left>', ':vertical resize +5<CR>', opts) -- +5 vertical
vim.keymap.set('n', '<M-Down>', ':horizontal resize -5<CR>', opts) -- -5 horizontal
vim.keymap.set('n', '<M-Up>', ':horizontal resize +5<CR>', opts) -- +5 horizontal
vim.keymap.set('n', '<M-Right>', ':vertical resize -5<CR>', opts) -- -5 vertical

-- MISC
vim.keymap.set('n', '?', ':nohl<CR>', opts) -- clear hightlights
vim.keymap.set('n', '<M-/>', ':set wrap!<CR>', opts) -- toggle wrap
vim.keymap.set('n', '<M-j>', ':m .+1<CR>', opts) -- move line down
vim.keymap.set('n', '<M-k>', ':m .-2<CR>', opts) -- move line up
vim.keymap.set('n', '<C-f>', ':/\\<\\>' .. back(2), noremap) -- find a term
vim.keymap.set('n', '<Leader>s', ':%s/\\<<C-r><C-w>\\>//g' .. back(2), noremap) -- replace a term
vim.keymap.set('n', '<Leader>S', ':cfdo %s/\\<\\>//g | update' .. back(14), noremap) -- replace a term across quick fix list
vim.keymap.set('n', '<Left>', '<Nop>', opts)
vim.keymap.set('n', '<Down>', '<Nop>', opts)
vim.keymap.set('n', '<Up>', '<Nop>', opts)
vim.keymap.set('n', '<Right>', '<Nop>', opts)
vim.keymap.set('n', '<LeftMouse>', '<Nop>', opts)
vim.keymap.set('n', '<RightMouse>', '<Nop>', opts)
vim.keymap.set('n', '<ScrollWheelUp>', '<Nop>', opts)
vim.keymap.set('n', '<ScrollWheelDown>', '<Nop>', opts)
vim.keymap.set('n', '<Leader><C-t>', ':' .. TERM .. '<CR><CR>', opts) -- open external TERM
vim.keymap.set('n', '<Leader><C-M-t>', ':' .. TERMHERE .. '<CR><CR>', opts) -- open external TERM at current file
vim.keymap.set('n', '<Leader><C-d>', ':' .. FILE .. '<CR><CR>', opts) -- open external file manager
vim.keymap.set('n', '<Leader><C-M-d>', ':' .. FILEHERE .. '<CR><CR>', opts) -- open external file manager at current file
vim.keymap.set('t', '<Esc>', '<C-\\><C-n>') -- free cursor from TERM
