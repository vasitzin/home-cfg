vim.schedule(function() vim.cmd 'LspStart' end)

require 'settings'
require 'keymappings'
require 'plugins'

vim.api.nvim_create_autocmd({ 'TermOpen' }, { command = 'setlocal nonumber norelativenumber' })

vim.api.nvim_create_autocmd({ 'BufWinEnter', 'TabEnter' }, { command = 'BDelete hidden regex=^$' })
