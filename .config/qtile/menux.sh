#!/usr/bin/sh

TERM="xterm-256color"
monitor=0
sb="#FFFFFF"
nf="#FFFFFF"
nb="#000000"

excluderoot="^caja-.*|^gcr-.*|^geoclue.*"
excludehome="^wine|.*-handler.*|\.cache$"

while getopts 'm:s:n:b:' opt; do
  case "$opt" in
    m) monitor="$OPTARG";;
    s) sb="$OPTARG";;
    n) nf="$OPTARG";;
    b) nb="$OPTARG";;
  esac
done; shift "$(($OPTIND - 1))"

apps=( $(ls /usr/share/applications | sed -e 's/\.desktop$//g' | grep -vE "$excluderoot" | rev | cut -d\. -f1 | rev | tr '[:upper:]' '[:lower:]') )
apps+=( $(ls $HOME/.local/share/applications | grep -vE "$excludehome" | sed -e 's/\.desktop$//g' | rev | cut -d\. -f1 | rev | tr '[:upper:]' '[:lower:]') )
last_pkg=${#apps[@]}	

apps+=( $(flatpak list --columns=name --app | sed -e 's/ /-/g' | tail -n +1 | tr '[:upper:]' '[:lower:]') )
last_fpk=${#apps[@]}	

appid=( $(ls /usr/share/applications | sed -e 's/\.desktop$//g' | grep -vE "$excluderoot") )
appid+=( $(ls $HOME/.local/share/applications | grep -vE "$excludehome") )
appid+=( $(flatpak list --columns=app --app | tail -n +1) )

choice=$( printf '%s\n' "${apps[@]}" | dmenu -m $monitor -i -fn 'FreeSans Bold - 16' -sf $nb -sb $sb -nf $nf -nb $nb -c -bw 2 -cw 120 -l 25 )
[ "${choice}" == "" ] && exit 90

pos=-1
for i in ${!apps[@]}; do
  [ "${apps[$i]}" == "$choice" ] && pos=$i
done
[ ${pos} -eq -1 ] && exit 91

if [ $pos -ge 0 -a $pos -lt $last_pkg ]; then
  exec gtk-launch ${appid[$pos]} &
  disown
elif [ $pos -ge $last_pkg -a $pos -lt $last_fpk ]; then
  exec flatpak run ${appid[$pos]} &
  disown
else
  exit 99
fi
