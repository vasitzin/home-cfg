# -*- coding: utf-8 -*-

import subprocess
from libqtile.config import Click, Drag, Key, KeyChord
from libqtile.lazy import lazy, LazyCall


def Kee(modz: list[str], keyz: list[str], *cmd: LazyCall):
  return [Key(modz, "{}".format(i), *cmd) for i in keyz]


def KeeChurd(modz: list[str], keyz: list[str], binds: list[Key | KeyChord]):
  return [KeyChord(modz, "{}".format(i), binds) for i in keyz]


@lazy.function
def float_to_front(qtile):
  for window in qtile.current_group.windows:
    if window.floating:
      window.bring_to_front()


@lazy.function
def menux(qtile, conf: str, colors: list[list[str]]):
  subprocess.run("{}/menux.sh -m {} -s {} -n {} -b {}".format(
    conf,
    qtile.current_screen.index,
    colors[3][0],
    colors[6][0],
    colors[0][0],
  ).split())


@lazy.function
def menu(qtile, conf: str, colors: list[list[str]]):
  subprocess.run("{}/menu.sh -m {} -s {} -n {} -b {}".format(
    conf,
    qtile.current_screen.index,
    colors[3][0],
    colors[6][0],
    colors[0][0],
  ).split())


class Keys:

  def __init__(self, monitors: int, groups: int, indexer: list[dict], home: str, conf: str, term: str, colors: list):
    self.monitors = monitors
    self.groups = groups
    self.indexer = indexer
    self.home = home
    self.conf = conf
    self.term = term
    self.colors = colors

  def getKeys(self, SPR, SFT, CTL, ALT):
    res = [
      Key([SPR], "Delete", lazy.shutdown()),
      Key([SPR, SFT], "Delete", lazy.restart()),
      Key([SPR], "space", lazy.widget["keyboardlayout"].next_keyboard()),
      Key([SPR], "Tab", lazy.next_layout()),
      Key([SPR, SFT], "Tab", lazy.prev_layout()),
      Key([], "Menu", menux(self.conf, self.colors)),
      Key([CTL], "Menu", menu(self.conf, self.colors)),
      *Kee([SPR], ["w", "Greek_finalsmallsigma"], lazy.window.kill()),
      *Kee([SPR], ["e", "Greek_epsilon"], lazy.window.toggle_maximize()),
      *Kee([SPR], ["f", "Greek_phi"], lazy.window.toggle_fullscreen()),
      *Kee([SPR], ["v", "Greek_omega"], lazy.window.toggle_floating()),
      *Kee([SPR], ["z", "Greek_zeta"], float_to_front),
      *Kee([SPR], ["j", "Greek_xi"], lazy.group.next_window()),
      *Kee([SPR], ["k", "Greek_kappa"], lazy.group.prev_window()),
      *Kee([SPR, SFT], ["j", "Greek_xi"], lazy.layout.shuffle_down()),
      *Kee([SPR, SFT], ["k", "Greek_kappa"], lazy.layout.shuffle_up()),
      *Kee([SPR], ["h", "Greek_eta"], lazy.layout.shrink(), lazy.layout.decrease_nmaster()),
      *Kee([SPR], ["l", "Greek_lamda"], lazy.layout.grow(), lazy.layout.increase_nmaster()),
      *Kee([SPR], ["n", "Greek_nu"], lazy.layout.reset()),
      *Kee([SPR, CTL], ["r", "Greek_rho"], self.__cmdfloat('htop')),
      *Kee([SPR, CTL, SFT], ["r", "Greek_rho"], self.__cmdfloat('nvtop')),
      *[Key([SPR], str(i), *self.__gotogroup(i)) for i in [*range(1, self.groups + 1)]],
      *[Key([SPR, CTL], str(i), lazy.window.togroup(str(i))) for i in [*range(1, self.groups)]],
      *[Key([SPR, CTL, SFT], str(i), *self.__grabtogroup(i)) for i in [*range(1, self.groups + 1)]],
    ]
    if self.monitors > 1:
      res.extend([Key([SPR, SFT], str(i['screen'] + 1), lazy.to_screen(i['screen'])) for i in self.indexer])
      res.extend([Key([SPR, ALT], str(i['screen'] + 1), lazy.window.toscreen(i['screen'])) for i in self.indexer])
      res.extend([Key([SPR, ALT, SFT], str(i['screen'] + 1), *self.__grabtoscreen(i['screen'])) for i in self.indexer])
    return res

  def getMouse(self, SPR):
    return [
      Click([SPR], "Button1", lazy.window.bring_to_front()),
      Drag([SPR], "Button1", lazy.window.set_position(), start=lazy.window.get_position()),
      Drag([SPR], "Button3", lazy.window.set_size_floating(), start=lazy.window.get_size()),
    ]

  def __cmdfloat(self, cmd: str):
    schema = "qtile run-cmd -f {} --config-file {}/.config/{}/floating.toml -e {}"
    return lazy.spawn(schema.format(self.term, self.home, self.term, cmd).split())

  def __gotogroup(self, group: int):
    res = []
    for i in self.indexer:
      if group in i['groups']:
        res.append(lazy.to_screen(i['screen']))
        res.append(lazy.group[str(group)].toscreen())
        break
    return res

  def __grabtogroup(self, group: int):
    res = []
    for i in self.indexer:
      if group in i['groups']:
        res.append(lazy.window.togroup(str(group)))
        res.append(lazy.to_screen(i['screen']))
        res.append(lazy.group[str(group)].toscreen())
        break
    return res

  def __grabtoscreen(self, screen: int):
    return [
      lazy.window.toscreen(screen),
      lazy.to_screen(screen),
    ]
