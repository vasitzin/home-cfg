#!/usr/bin/sh

xrandr --listmonitors | tail -n +2 | cut -d\  -f4 | sed -z 's/\n/,/g'
