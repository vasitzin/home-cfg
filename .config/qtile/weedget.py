# -*- coding: utf-8 -*-

from libqtile import qtile, bar, hook, pangocffi
from libqtile.widget import base
from libqtile.log_utils import logger


class WeendowName(base._TextBox):
  defaults = [
    ("for_current_screen", False, ""),
    ("empty_group_string", " ", ""),
    ("format", "{state}{name}", ""),
    ("parse_text", None, ""),
  ]

  def __init__(self, width=bar.STRETCH, **config):
    base._TextBox.__init__(self, width=width, **config)
    self.add_defaults(WeendowName.defaults)

  def _configure(self, qtile, bar):
    base._TextBox._configure(self, qtile, bar)
    hook.subscribe.client_name_updated(self.hook_response)
    hook.subscribe.focus_change(self.hook_response)
    hook.subscribe.float_change(self.hook_response)
    hook.subscribe.current_screen_change(self.hook_response_current_screen)

  def remove_hooks(self):
    hook.unsubscribe.client_name_updated(self.hook_response)
    hook.unsubscribe.focus_change(self.hook_response)
    hook.unsubscribe.float_change(self.hook_response)
    hook.unsubscribe.current_screen_change(self.hook_response_current_screen)

  def hook_response(self, *args):
    if self.for_current_screen:
      w = self.qtile.current_screen.group.current_window
    else:
      w = self.bar.screen.group.current_window
    state = "󰝘  "
    if w:
      if w.maximized:
        state = "󰖯  "
      elif w.minimized:
        state = "󰖰  "
      elif w.floating:
        state = "󰖲  "
      var = {}
      var["state"] = state
      var["name"] = w.name
      if callable(self.parse_text):
        try:
          var["name"] = self.parse_text(var["name"])
        except:
          logger.exception("parse_text function failed:")
      wm_class = w.get_wm_class()
      var["class"] = wm_class[0] if wm_class else ""
      unescaped = self.format.format(**var)
    else:
      unescaped = self.empty_group_string
    self.update(pangocffi.markup_escape_text(unescaped))

  def hook_response_current_screen(self, *args):
    if self.for_current_screen:
      self.hook_response()

  def finalize(self):
    self.remove_hooks()
    base._TextBox.finalize(self)
