#!/usr/bin/sh

TERM="xterm-256color"
monitor=0
sb="#FFFFFF"
nf="#FFFFFF"
nb="#000000"

while getopts 'm:s:n:b:' opt; do
  case "$opt" in
    m) monitor="$OPTARG";;
    s) sb="$OPTARG";;
    n) nf="$OPTARG";;
    b) nb="$OPTARG";;
  esac
done; shift "$(($OPTIND - 1))"

choice=$( dmenu_run -m $monitor -i -fn ' - 16' -sf $nb -sb $sb -nf $nf -nb $nb -c -bw 2 -cw 120 -l 25 )
[ "${choice}" == "" ] && exit 90

eval "$choice & disown"
