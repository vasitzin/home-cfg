# -*- coding: utf-8 -*-

import os, subprocess, re
from screens import Screens
from keys import Keys
from libqtile import qtile, layout, hook
from libqtile.config import Match, Group

o_home = os.path.expanduser('~')
o_conf = os.path.expanduser('~/.config/qtile')
o_term = 'alacritty'
o_colors = [
  ["#282A36", "#282A36"],
  ["#F8F8F2", "#F8F8F2"],
  ["#1C1D26", "#1C1D26"],
  ["#BD93F9", "#BD93F9"],
  ["#A66DF7", "#A66AF7"],
  ["#7822f3", "#7822f3"],
  ["#8BE9FD", "#8BE9FD"],
  ["#17D3FB", "#17D3FB"],
  ["#039FC1", "#039FC1"],
]

c_dunstify = "dunstify -u NORMAL -h string:x-dunst-stack-tag:qtile-internal".split()
c_getmouselocation = "xdotool getmouselocation".split()
c_mousemove = "xdotool mousemove --sync --".split()

subprocess.call(["{}/restart.sh".format(o_conf)])

o_groups = 9
o_screens = subprocess.run(["{}/getscreens.sh".format(o_conf)], stdout=subprocess.PIPE)
o_screens = str(o_screens.stdout).split("'")[1]
o_monitors = subprocess.run(["{}/getmonitors.sh".format(o_conf)], stdout=subprocess.PIPE)
o_monitors = int(o_monitors.stdout)
o_prev_screen = 0
o_index = []

l_screens_info = o_screens.split(',')
l_groups_per = o_groups // o_monitors
l_last = 0
for monitor in [*range(0, o_monitors)]:
  matching_groups = []
  for group in [*range(l_last + 1, l_last + l_groups_per + 1)]:
    matching_groups.append(group)
    l_last = group
  screen_space = l_screens_info[monitor].split('+')[0]
  o_index.append(
    dict(
      screen=monitor,
      width=int(screen_space.split('x')[0].split('/')[0]),
      height=int(screen_space.split('x')[1].split('/')[0]),
      xoffset=int(l_screens_info[monitor].split('+')[1]),
      yoffset=int(l_screens_info[monitor].split('+')[2]),
      groups=matching_groups,
    ))
del l_last
del l_groups_per
del l_screens_info


@hook.subscribe.startup_once
def start():
  l_startup = "{}/.startup.sh".format(o_home)
  subprocess.call([l_startup])
  subprocess.call(
    [*c_mousemove,
     str(o_index[0]['xoffset'] + o_index[0]['width'] // 2),
     str(o_index[0]['yoffset'] + o_index[0]['height'] // 2)])


# @hook.subscribe.screen_change
# def change(_):
#   qtile.spawn("qtile cmd-obj -o cmd -f restart".split())


@hook.subscribe.current_screen_change
def mouse_to_screen():
  global o_prev_screen
  l_current_screen = int(qtile.current_screen.index)
  l_mouse = subprocess.run(c_getmouselocation, stdout=subprocess.PIPE)
  l_mouse = str(l_mouse.stdout).split("'")[1].split()
  l_mouse_x = (int(l_mouse[0].split(":")[1]) - o_index[o_prev_screen]['xoffset']) / o_index[o_prev_screen]['width']
  l_mouse_y = (int(l_mouse[1].split(":")[1]) - o_index[o_prev_screen]['yoffset']) / o_index[o_prev_screen]['height']
  if not l_mouse_x >= 1.0 and not l_mouse_y >= 1.0 and not l_mouse_x <= 0.0 and not l_mouse_y <= 0.0:
    l_new_x = int(o_index[l_current_screen]['xoffset'] + int(float(o_index[l_current_screen]['width']) * l_mouse_x))
    l_new_y = int(o_index[l_current_screen]['yoffset'] + int(float(o_index[l_current_screen]['height']) * l_mouse_y))
    subprocess.call([*c_mousemove, str(l_new_x), str(l_new_y)])
  o_prev_screen = l_current_screen


# Layouts
o_layout_theme = {
  "margin": 6,
  "border_width": 2,
  "border_focus": o_colors[3][0],
  "border_normal": o_colors[8][0],
}
layouts = [
  layout.MonadTall(**o_layout_theme),
  layout.MonadWide(**o_layout_theme),
  layout.Floating(**o_layout_theme),
]
floating_layout = layout.Floating(
  **o_layout_theme,
  float_rules=[
    *layout.Floating.default_float_rules,
    Match(role='pop-up', wm_class='brave-browser'),
    Match(wm_class='zenity'),
    Match(wm_class='Osmo'),
    Match(wm_class='pavucontrol'),
    Match(wm_class='blueman-manager'),
    Match(wm_class='engrampa'),
    Match(wm_class='keepassxc'),
    Match(wm_class='gpicview'),
    Match(wm_class='lutris'),
    Match(wm_class='steam'),
    Match(wm_class='kruler'),
    Match(wm_class=re.compile('pinentry.*')),
    Match(wm_class=re.compile('Qalculate.*')),
    Match(wm_class=re.compile('transmission.*')),
    Match(title=re.compile('.* - mpv')),
    Match(title=re.compile('.*Select an IWAD to use.*')),  #GZDoom dialog
  ])
default_layout = [Match('layout', 'monadtall')]

# Groups
temp_groups = []
for group in [*range(1, o_groups + 1)]:
  for monitor in o_index:
    if group in monitor['groups']:
      temp_groups.append(Group("{}".format(group), default_layout, screen_affinity=monitor['screen']))
      break
groups = temp_groups
del temp_groups

# Screens
widget_defaults = dict(font='FreeSans Bold', fontsize=16, padding=3, background=o_colors[0])
screens = Screens(o_index, o_home, o_term, o_colors).getScreens()

# Keys
SPR = 'mod4'  # Universal
SFT = 'shift'  # Alternatives
CTL = 'control'  # Programms
ALT = 'mod1'  # Scripts
my_keys = Keys(o_monitors, o_groups, o_index, o_home, o_conf, o_term, o_colors)
keys = my_keys.getKeys(SPR, SFT, CTL, ALT)
mouse = my_keys.getMouse(SPR)
del my_keys

# Options
reconfigure_screens = False
focus_on_window_activation = "smart"
bring_front_click = "floating_only"
floats_kept_above = True
follow_mouse_focus = False
cursor_warp = False
wmname = "LG3D"
