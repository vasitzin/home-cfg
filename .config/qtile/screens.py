# -*- coding: utf-8 -*-

from libqtile import qtile, widget, bar
from libqtile.config import Screen
from weedget import WeendowName

o_fonticon = 'Font Awesome 6 Free Regular'  # icon only font
o_fontmono = 'JetBrainsMono Bold'  # text only
o_fontmix = 'JetBrainsMono NF Bold'  # mixed font
o_height = 24  # height of the bars
o_inval = 1.66  # interval to be used in polling options
o_netstr = "[D:{down:6.2f}|U:{up:6.2f}]"  #{down_suffix:2} {up_suffix:2} ⬇⬆▼▲


class Screens:

  def __init__(self, indexer: list[dict], home: str, term: str, colors: list):
    self.indexer = indexer
    self.home = home
    self.term = term
    self.colors = colors

  def getScreens(self):
    l_screens = []
    for i in self.indexer:
      if i['screen'] != 0:
        l_screens.append(self._extra(i['screen']))
      elif i['screen'] == 0:
        l_screens.append(self._prime(i['screen']))
    return l_screens

  def _prime(self, screen):
    return Screen(
      wallpaper='~/Pictures/backgrounds/aesthetic00.jpg',
      wallpaper_mode='stretch',
      top=bar.Bar(
        [
          *self.__group_box(screen),
          widget.Sep(),
          widget.CurrentLayoutIcon(scale=0.8),
          widget.CurrentLayout(),
          widget.Sep(),
          WeendowName(),
          widget.WidgetBox(
            font=o_fonticon,
            text_closed="",
            text_open="",
            close_button_location="right",
            widgets=[
              widget.LaunchBar(
                font=o_fonticon,
                progs=[
                  ('', 'poweroff', 'Poweroff'),
                  ('', 'reboot', 'Reboot'),
                  ('', 'logout', 'Quit Qtile'),  # shell script inside ~/.local/bin, part of PATH
                ],
              ),
              widget.Sep(),
            ],
          ),
          widget.Systray(),
          widget.Sep(),
          widget.WidgetBox(
            foreground=self.colors[3],
            font=o_fontmix,
            text_closed="",
            text_open="",
            close_button_location="right",
            widgets=[
              *self.__wlan("wlan0", 3),
              widget.Sep(),
              *self.__lan("enp9s0", 6),
              widget.Sep(),
            ],
          ),
          widget.Sep(),
          *self.__signal("wlan0", 6),
          widget.Sep(),
          *self.__kb_layout(3),
          widget.Sep(),
          *self.__volume(6),
          widget.Sep(),
          widget.Clock(
            format="%Y/%m/%d %A %R",
            foreground=self.colors[1],
            mouse_callbacks={"Button1": self.__calendar__},
          ),
        ],
        o_height,
      ),
    )

  def _extra(self, screen: int):
    return Screen(
      wallpaper='~/Pictures/backgrounds/aesthetic00.jpg',
      wallpaper_mode='stretch',
      top=bar.Bar(
        [
          *self.__group_box(screen),
          widget.Sep(),
          widget.CurrentLayoutIcon(scale=0.8),
          widget.CurrentLayout(),
          widget.Sep(),
          WeendowName(),
          *self.__wlan("wlan0", 3),
          widget.Sep(),
          *self.__lan("enp9s0", 6),
          widget.Sep(),
          *self.__kb_layout(3),
          widget.Sep(),
          *self.__volume(6),
          widget.Sep(),
          widget.Clock(
            format="%Y/%m/%d %A %R",
            foreground=self.colors[1],
            mouse_callbacks={"Button1": self.__calendar__},
          ),
        ],
        o_height,
      ),
    )

  def __iwctl__(self):
    cmd = "qtile run-cmd -f {} --config-file {}/.config/{}/floating.toml -e iwctl"
    qtile.spawn(cmd.format(self.term, self.home, self.term).split())

  def __mixer__(self):
    qtile.spawn('pavucontrol')

  def __calendar__(self):
    qtile.spawn("osmo -c".split())

  def __group_box(self, screen: int):
    return [
      widget.CurrentScreen(
        font=o_fontmix,
        fontsize=17,
        active_text=":{}".format(screen + 1),
        active_color=self.colors[3],
        inactive_text=":{}".format(screen + 1),
        inactive_color=self.colors[7],
      ),
      widget.Sep(),
      widget.CurrentScreen(
        font=o_fontmix,
        fontsize=17,
        active_text=":".format(screen + 1),
        active_color=self.colors[3],
        inactive_text=":".format(screen + 1),
        inactive_color=self.colors[7],
      ),
      widget.GroupBox(
        visible_groups=[str(g) for g in self.indexer[screen]['groups']],
        highlight_method='block',
        active=self.colors[1],
        inactive=self.colors[2],
        this_current_screen_border=self.colors[4],
        other_screen_border=self.colors[5],
        this_screen_border=self.colors[8],
        other_current_screen_border=self.colors[7],
        padding=8,
        rounded=False,
        center_aligned=True,
        disable_drag=True,
        use_mouse_wheel=False,
        toggle=False,
      ),
    ]

  def __kb_layout(self, code: int):
    return [
      widget.TextBox(
        font=o_fonticon,
        text="",
        foreground=self.colors[code],
      ),
      widget.KeyboardLayout(
        font=o_fontmono,
        configured_keyboards=["us", "gr"],
        foreground=self.colors[code],
      )
    ]

  def __volume(self, code: int):
    return [
      widget.TextBox(
        font=o_fonticon,
        text="",
        foreground=self.colors[code],
        mouse_callbacks={"Button1": self.__mixer__},
      ),
      widget.Volume(
        step=5,
        update_interval=o_inval,
        foreground=self.colors[code],
      )
    ]

  def __lan(self, iface, code: int):
    return [
      widget.TextBox(
        font=o_fonticon,
        text="",
        foreground=self.colors[code],
      ),
      widget.Net(
        font=o_fontmix,
        prefix='M',
        interface=iface,
        use_bits=True,
        update_interval=o_inval,
        format=o_netstr,
        foreground=self.colors[code],
      )
    ]

  def __wlan(self, iface, code: int):
    return [
      widget.TextBox(
        font=o_fonticon,
        text="",
        foreground=self.colors[code],
        mouse_callbacks={"Button1": self.__iwctl__},
      ),
      widget.Net(
        font=o_fontmono,
        prefix='M',
        interface=iface,
        use_bits=True,
        update_interval=o_inval,
        format=o_netstr,
        foreground=self.colors[code],
      )
    ]

  def __signal(self, iface, code: int):
    return [
      widget.TextBox(
        font=o_fonticon,
        text="",
        foreground=self.colors[code],
        mouse_callbacks={"Button1": self.__iwctl__},
      ),
      widget.Wlan(
        font=o_fontmono,
        interface=iface,
        disconnected_message="--%",
        format="{percent:1.0%}",
        update_interval=o_inval,
        foreground=self.colors[code],
      )
    ]
