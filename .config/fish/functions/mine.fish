#!/usr/bin/env fish

function mine
    argparse h/help 'c/coin=' 't/threads=' 'm/mask=' 'e/ethash=' s/stress -- $argv

    if set -q _flag_h
        echo ' -c/--coin= [STRING]'\t'crypto currency eg xmr'\n \
            '-t/--threads= [NUM]'\t'number of threads to use'\n \
            '-m/--mask= [NUMS]'\t'range number of cores to use eg 0-3,7'\n \
            '-e/--ethash= [STRING]'\t'gpu intensity config eg: XB512'\n \
            '-s/--stress'\t\t'stress test only xmrig'
        return
    else if begin
            ! set -q _flag_c
        end
        echo 'Need to specify a coin.'
        return 1
    end

    set -l poolXmr 'xmr.2miners.com:12222' 'pool.minexmr.com:443'
    set -l poolRvn 'rvn.2miners.com:16060'
    set -l walletXmr 4A4iJVUm7mE8NxJqh6RBncJ7R1HJQsJSN4WEm49MkyMcFdzxTX9H3ut4ufjBZ1VbMa7tXQ844cD2P8MywvpLM6J4AbQLd6F
    set -l walletRvn RETCudSZBAvgqnzRqExzKBgu9LFpR5fzHs

    set -l s (date +%s)
    switch $_flag_c
        case xmr
            if ! set -q _flag_m
                echo 'Need to specify core util mask eg 0-4,7,9.'
                return 1
            end
            set -l str (maskitty --mask=$_flag_m -t)
            set -l threads (echo "$str" | awk '{print $1}')
            set -l mask (echo "$str" | awk '{print $2}')
            doas xmrig --algo="rx/0" --threads=$threads --cpu-affinity=0x$mask --config="$XDG_CONFIG_HOME/xmr.conf" (if set -q _flag_s; echo '--stress'; end) 2>"$LOGS/xmr.log"
        case rvn
            doas teamredminer --disable_colors -o stratum+ssl://$poolRvn[1] -u $walletRvn[1] -a kawpow --fan_control=75:::60:60:90 2>"$LOGS/rvn.log"
        case '*'
            echo 'Unknown coin!'
            return 1
    end
    math (math (date +%s) - $s) / 3600
end
