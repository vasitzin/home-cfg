#!/usr/bin/env fish

function setsink -d "Change default pulseaudio sink to specified sink" -a newDefault newTitle
    pactl set-default-sink $newDefault
    for index in (pactl list-sink-inputs | awk '/index/ {print $2}')
        patl move-sink-input $index $newDefault
    end
end
