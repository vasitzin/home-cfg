#!/usr/bin/env fish

function gitlabup
    argparse f/file= p/project= -- $argv
    set token (read -p 'echo "Provide your access token: "')
    echo "curl --request POST --header PRIVATE-TOKEN: $token --form file=@$_flag_f $_flag_p"
    curl --request POST --header "PRIVATE-TOKEN: $token" --form "file=@$_flag_f" "$_flag_p"
end
