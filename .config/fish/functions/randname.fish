#!/usr/bin/env fish

function randname
    argparse c/chmod -- $argv

    set containers 'find . -maxdepth 1 -type f'

    set files (eval $containers | sort)

    for f in $files
        set extension (echo $f | awk -F '.' '{ printf $NF }' | cut -c1-)
        set newName 0x
        while test (echo $newName | grep -E "^[0-9]")
            set newName (keepassxc-cli generate -L 12 -l -n)
            set newName "$newName.$extension"
        end
        if set -q _flag_c
            chmod 644 "$f"
        end
        mv "$f" "$newName"
    end
end
