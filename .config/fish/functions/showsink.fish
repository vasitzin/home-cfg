#!/usr/bin/env fish

function showsink -d "Displays current default sink in pulseaudio"
    set default (pactl get-default-sink)
    set sinks (pactl list short sinks | awk '{print $2}')
    set sinkTitles (pactl list sinks | grep Description | cut -c15-)
    set current 1

    for sink in $sinks
        if test $sink = $default
            set currentTitle $sinkTitles[$current]
            break
        end
        set current (math $current + 1)
    end

    notify-send -u NORMAL -i audio-headphones-symbolic.symbolic -a audio 'You are listening through:' $currentTitle
end
