#!/usr/bin/env fish

function ytchannelid
    argparse "u/url=" -- $argv

    if begin
            not set -q _flag_u
        end
        return 1
    end

    curl --no-progress-meter "$_flag_u" > response.html
    prettier response.html | grep rssUrl | rev | cut -c3- | rev | awk -F ': "' '{print $2}'
    rm -rf response.html
end
