#!/usr/bin/env fish

function txtconv -d 'Convert text file to utf-8'
    argparse 'i/file=' a/all -- $argv

    if begin
            set -q _flag_i
            and set -q _flag_a
        end
        return 1
    end

    set files
    if set -q _flag_i
        set files $_flag_i
    else if set -q _flag_a
        set files (find . -maxdepth 1 -type f | cut -c3-)
    end

    for f in $files
        set char (uchardet "$f")
        if test $status -eq 0 -a $char != ''
            iconv -f $char -t UTF-8 -o "&$f" $f
        else
            echo "Charset detection error: $f, $char."
        end
    end
end
