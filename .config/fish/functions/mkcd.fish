#!/usr/bin/env fish

function mkcd
  mkdir $argv
  cd $argv[-1]
end
