#!/usr/bin/env fish

function bkp
    argparse 'd/device=' 'a/address=' 'p/port=' -- $argv

    if begin
            not set -q _flag_d
        end
        echo 'The device needs to be specified -d/--device='
        return 1
    end

    if begin
            not set -q _flag_a
        end
        echo 'The address needs to be specified -a/--address='
        return 1
    end

    switch $_flag_d
        case euterpi
            rsync --archive --delete --info=progress2 --no-i-r \
                --include={'/.config/','/.var/','/.gnupg/','/.ssh/','/Vaults/'} \
                --exclude={'/.*','/Android','/Desktop/*','/Downloads/*','/Torrents/*','/Images/*','/Videos/movies/*','/Videos/series/*','*/cache/*','*/caches/*'} \
                -e ssh /home/vasilis/ vasilis@$_flag_a:/home/vasilis/bkp
        case marika
            rsync --archive --info=progress2 --no-i-r \
                --include={'/Backups/***','/DCIM/***','/Pictures/***'} \
                --exclude '*' \
                /run/user/1000/$_flag_a/storage/emulated/0/ $HOME/Mobile
        case '*'
            return -1
    end
end
