#!/usr/bin/env fish

function bare -w git -d 'Git wrap for bare repositories'
    set -l old (pwd)
    switch $argv[1]
        case root
            cd /
            if test $argv[2] = sudo
                sudo git --git-dir=$HOME"/Repos/bare/root" --work-tree="/" $argv[3..-1]
            else if test $argv[2] = doas
                doas git --git-dir=$HOME"/Repos/bare/root" --work-tree="/" $argv[3..-1]
            else
                git --git-dir=$HOME"/Repos/bare/root" --work-tree="/" $argv[2..-1]
            end
        case home
            cd $HOME
            git --git-dir=$HOME"/Repos/bare/home" --work-tree=$HOME $argv[2..-1]
        case status
            echo "\$ bare root status"
            cd /
            git --git-dir=$HOME"/Repos/bare/root" --work-tree="/" status
            echo
            echo "\$ bare home status"
            cd $HOME
            git --git-dir=$HOME"/Repos/bare/home" --work-tree=$HOME status
        case *
            return 1
    end
    cd $old
end
