#!/usr/bin/env fish

function chsink -d 'Cycle through sinks in the system'
    argparse 'd/direction=' -- $argv

    if begin
            set -q _flag_d
        end
    else
        echo 'A direction -d/--direction= needs be specified (f or b).'
        return 1
    end

    set default (pactl get-default-sink) # Default pulseaudio sink at script start
    set sinks (pactl list short sinks | awk '{print $2}' | grep -v audio.share) # Array of current sinks present in the system
    set sinkTitles (pactl list sinks | grep Description | cut -c15-) # and their human readable names
    set sinkSum (count $sinks) # sum of all sinks in the system

    switch $_flag_d
        case f # cycles to the next available sink
            if test $default = $sinks[$sinkSum]
                set newDefault $sinks[1]
                set newTitle $sinkTitles[1]
            else
                set next 2 # position of next sink in sinks array
                for sink in $sinks # go through each sink in sinks array
                    if test $sink = $default
                        set newDefault $sinks[$next]
                        set newTitle $sinkTitles[$next]
                        break
                    end
                    set next (math $next + 1)
                end
            end
        case b # cycles to the prev available sink
            if test $default = $sinks[1]
                set newDefault $sinks[$sinkSum]
                set newTitle $sinkTitles[$sinkSum]
            else
                set prev (math $sinkSum - 1) # position of previous sink in sinks array
                for sink in $sinks[-1..1] # go through each sink in sinks array, but in reverse order
                    if test $sink = $default
                        set newDefault $sinks[$prev]
                        set newTitle $sinkTitles[$prev]
                        break
                    end
                    set prev (math $prev - 1)
                end
            end
    end

    setsink $newDefault $newTitle
end
