#!/usr/bin/env fish

function up
    if test -f /usr/bin/doas
        set su doas
    else if test -f /usr/bin/sudo
        set su sudo
    else
        return 99
    end

    set -l allargs (count $argv)
    argparse d/distro u/aur f/flatpak g/git n/npm v/vim S/system k/kernel -- $argv
    if test (count $argv) -eq $allargs
        set _flag_d
        set _flag_u
        set _flag_f
        set _flag_g
        set _flag_n
        set _flag_v
    else if set -q _flag_S
        set _flag_d
        set _flag_u
        set _flag_f
    end

    set dontclose 0

    if set -q _flag_d #ARCH REPOS
        echo -en "\033[1;30;42m"Arch"\033[0m\n"
        begin
            $su pacman -Syu --noconfirm
            or set dontclose 1
        end
        echo
    end

    if set -q _flag_k #ARCH IGNORED
        echo -en "\033[1;30;42m"Arch Ignored Packages"\033[0m\n"
        begin
            eval "$su pacman -S --needed --noconfirm $(cat /etc/pacman.conf | awk -F '=' '/^IgnorePkg/ {print $2}' | cut -c2-)"
            or set dontclose 1
        end
        echo
    end

    if set -q _flag_u #AUR
        echo -en "\033[1;30;42m"AUR"\033[0m\n"
        begin
            paru -Syua
            or set dontclose 1
        end
        echo
    end

    if set -q _flag_f #FLATPAK
        echo -en "\033[1;30;42m"Flatpak"\033[0m\n"
        begin
            flatpak update --assumeyes
            or set dontclose 1
        end
        echo
    end

    if set -q _flag_g #GIT REPOS
        echo -en "\033[1;30;42m"Git Repos"\033[0m\n"
        begin
            gogitit
            or set dontclose 1
        end
        echo
    end

    if set -q _flag_n #NPM
        echo -en "\033[1;30;42m"NPM"\033[0m\n"
        begin
            $su npm -g up
            or set dontclose 1
        end
        echo
    end

    if set -q _flag_v #VIM
        echo -en "\033[1;30;42m"VIM"\033[0m\n"
        begin
            echo TSInstall:
            nvim --headless -c 'TSInstallSync all' -c qa
            echo -en "\n"
            echo LazyUpdate:
            nvim --headless -c 'Lazy! update' -c qa | grep -E "HEAD is now at|!:"
            echo -en "\n"
            echo TSUpdateSync:
            nvim --headless -c TSUpdateSync -c qa
        end
        echo
    end

    if begin
            set -q _flag_d
            or set -q _flag_u
            or set -q _flag_f
        end
        echo
        echo -en "\033[1;30;42m"Cleanup"\033[0m\n"

        if begin
                set -q _flag_d
                or set -q _flag_u
            end
            echo -n "Paru:"
            begin
                paru --clean
                or set dontclose 1
            end
        end

        if set -q _flag_f
            echo -n "Flatpak: "
            begin
                flatpak uninstall --delete-data --unused --assumeyes
                or set dontclose 1
            end
        end
    end

    if test $dontclose -eq 1
        play -q $SFX/metal-gear-alert-sound-effect.ogg &
        fish
    end
end
