#!/usr/bin/env fish

function killwine
    killall wine wineserver
    ps -f -U "$USER" | grep -E "C:.*\.exe" | grep -vE grep | awk '{ print $2 }' | xargs kill -9
end
