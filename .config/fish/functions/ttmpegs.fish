#!/usr/bin/env fish

function ttmpegs
    argparse h/help 'f/format=' 'p/pass=' a/all -- $argv

    if set -q _flag_h
        echo ' {REQUIRED}'\n \
            '-f/--format= [FORMAT]'\t'output format'\n \
            '-p/--pass= [FLAGS]'\t'flags to pass to ttmpeg (eg -p "-s -a -e")'\n \
            '{OPTIONAL}'\n \
            '-a/--all'\t\t'run ttmpeg for all files in directory, normally excludes files in format same as specified in \'-f\''\n \
            '-h/--help'\t\t'ignore all options and print this menu'
        echo \n' {ttmpeg --help}'
        ttmpeg -h
        return
    else if begin
            ! set -q _flag_f
            or ! set -q _flag_p
        end
        echo 'Need to specify an output format -f/--format and ttmpeg flags -p/--pass.'
        return 1
    end

    set files
    set containers 'find . -maxdepth 1 -type f | grep -E "mkv\$|mp4\$|m4v\$|wmv\$|avi\$|ogv\$|divx\$|flv\$|webm\$|gif\$"'
    if set -q _flag_a
        set files (eval $containers | sort)
    else
        set files (eval $containers | grep -vE $_flag_f'$' | sort)
    end

    set total (count $files)

    set count 0
    for f in $files
        set count (math $count + 1)
        set cmd "ttmpeg -f $_flag_f -i '$(echo $f | cut -c3-)' $_flag_p"
        echo -e "\n\033[1;30;42m$f \033[0m\033[1;32m [$count/$total]\033[0m"
        echo $cmd\n
        set start (date +%s)
        eval $cmd
        set evalstat $status
        set end (math (math (date +%s) - $start) / 60)
        if test $evalstat -eq 0
            echo -en "\033[1;32m"
        else
            echo -en "\033[1;31m"
        end
        echo -e "Completed in $end minutes.\033[0m"
    end
end
