#!/usr/bin/env fish

function df-all -d 'Print all drives capacity'
    echo -e 'Filesystem\t|Used\t|Avail\t|Mount Point'
    echo -e '----------\t|----\t|-----\t|-----------'
    df -h | awk '/^\/dev\// {print $1"\t|"$5"\t|"$4"\t|"$6}'
end
