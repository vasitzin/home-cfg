#!/usr/bin/env fish

function dl -w yt-dlp -d 'yt-dlp wrapper'
    argparse h/help s/skip b/best a/audio p/post= -- $argv

    if set -q _flag_h
        echo ' {REQUIRED}'\n \
            'none'\n \
            '{OPTIONAL}'\n \
            '-s/--skip'\t\t'skip format listing'\n \
            '-b/--best'\t\t'autoselect best quality video'\n \
            '-a/--audio'\t\t'audio only download'\n \
            '-p/--post'\t\t'ffmpeg postprocess command'\n \
            '-h/--help'\t\t'ignore all options and print this menu'
        return
    end

    set -l wd (pwd)
    set -l input
    set -l cmd

    cd $HOME/Downloads
    for arg in $argv
        echo
        if set -q _flag_b
            set input "\"bestvideo+bestaudio/best\" -S vcodec:avc1 -S acodec:opus --prefer-free-formats"
        else if set -q _flag_a
            if not set -q _flag_s
                yt-dlp -q --list-formats "$arg"
            end
            set input (read -p "echo -en \"\n\033[1mSelection ('+' to combine): \033[0m\"")
        else
            if not set -q _flag_s
                yt-dlp -q --list-formats "$arg"
            end
            set input (read -p "echo -en \"\n\033[1mSelection ('+' to combine): \033[0m\"")
        end

        if not set -q _flag_a
            set cmd "yt-dlp --all-subs -f $input --remux-video mkv"
        else
            set cmd "yt-dlp -x --audio-format opus -f $input"
        end

        if set -q _flag_p
            set cmd "$cmd --postprocessor-args \"$_flag_p\""
        end

        echo "$cmd -o \"%(title)s.%(ext)s\" \"$arg\""
        begin
            eval "$cmd -o \"%(title)s.%(ext)s\" \"$arg\""
            or play -q $SFX/metal-gear-alert-sound-effect.ogg &
        end
    end
    cd $wd
end
