#!/usr/bin/env fish

function nv -w nvim
    argparse 's/source' -- $argv
    if set -q _flag_s
        nvim -S Session.vim -- $argv
    else
        nvim $argv
    end
end
