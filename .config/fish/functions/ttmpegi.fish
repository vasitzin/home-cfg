#!/usr/bin/env fish

function ttmpegi
    argparse 'i/file=' -- $argv

    if ! set -q _flag_i
        echo 'Need to specify an input -i/--file.'
        return 1
    end

    set venc (ffprobe -v quiet -select_streams v:0 -show_entries stream=codec_name -of default=nokey=1:noprint_wrappers=1 $_flag_i)
    set aenc (ffprobe -v quiet -select_streams a:0 -show_entries stream=codec_name -of default=nokey=1:noprint_wrappers=1 $_flag_i)
    set streams (ffprobe -v quiet -show_entries stream=codec_type -of default=nw=1:nk=1 $_flag_i | uniq -c | tr -s [:space:])
    echo "file='$_flag_i'"
    echo \t"vencode=$venc aencode=$aenc"
    echo -n \t'streams:'
    for stream in $streams
        echo -n "$(echo $stream),"
    end
    echo
end
