#!/usr/bin/env fish

function ttmpeg
    argparse h/help 'i/file=' 'f/format=' \
        D/dry-run C/clear-chapters s/subs S/clear-subs 'H/height=' e/encode E/force-encode \
        'n/name=' r/replace -- $argv

    if set -q _flag_h
        echo ' {REQUIRED}'\n \
            '-i/--file= [INPUT] '\t'input video file'\n \
            '-f/--format= [FORMAT]'\t'output format'\n \
            '{OPTIONAL}'\n \
            '-D/--dry-run'\t\t'print the final command but do not execute it'\n \
            '-C/--clear-chapters'\t'clear all chapter metadata'\n \
            '-s/--subs'\t\t'replace subtitles with srt subtitle files from same dir (eg naming scheme "./eng@Life of Pi.srt")'\n \
            '-S/--clear-subs'\t\t'clear all subtitles'\n \
            '-H/--height= [PIXELS]'\t'change video scaling by setting height while width is auto selected to keep aspect ratio'\n \
            '-e/--encode'\t\t'change the encodings to predefined values'\n \
            '-E/--force-encode'\t're-encode even if the file uses the same encoding'\n \
            '-n/--name= [STRING]'\t\t'specify output file name without the extension'\n \
            '-r/--replace'\t\t'replace old file when done'\n \
            '-h/--help'\t\t'ignore all options and print this menu'
        return
    else if begin
            ! set -q _flag_i
            or ! set -q _flag_f
        end
        echo 'Need to specify an output format with -f/--format and an input file with -i/--file.'
        return 1
    end

    if begin
            set -q _flag_e
            and set -q _flag_E
        end
        set -e _flag_e
        return 2
    end

    set cv libsvtav1
    set cvaccept av1 hevc
    set cvflags '-preset 6 -crf 28 '

    set ca libopus
    set caaccept opus vorbis
    set caflags '-b:a 48k '

    set cs subrip
    set csaccept srt
    set csflags ''

    # figure out naming
    set input_name "$(echo $_flag_i | awk -F '.' '{ for (i=1; i<NF; ++i) { printf "."$i } }' | cut -c2-)"
    set output ''
    if set -q _flag_n
        set output "$_flag_n.$_flag_f"
    else
        set output "_$input_name.$_flag_f"
    end
    if test "$input_name" = '' -o "$output" = '' -o "$output" = ".$_flag_f"
        return 666
    end

    set cmd 'ffmpeg -hide_banner -loglevel warning -stats '

    # input files
    set cmd $cmd"-i '$_flag_i' "
    set subs
    if set -q _flag_s
        set subs (find . -maxdepth 1 -type f | cut -c3- | grep -E "^[a-z]{3}@$input_name\.srt\$" | sort)
        for sub in $subs
            set cmd $cmd"-i '$sub' "
        end
    end


    # map video and audio streams
    set cmd $cmd'-map 0:v:0 -map "0:a?" '
    for tag in (ffprobe -v quiet -show_entries stream=index:stream_tags=language \
                        -select_streams a -of compact=p=0:nk=1 $_flag_i | grep -E '^[0-9]*\|.*')
        set lang "$(echo $tag | awk -F '|' '{print $2}')"
        if test "$lang" != und
            set cmd $cmd"-metadata:s:$(echo $tag | awk -F '|' '{print $1}') "
            set cmd $cmd"language=$lang "
        end
    end

    if set -q _flag_s
        # map subtitle files
        set count 0
        for sub in $subs
            set count (math $count + 1)
            set lang "$(echo $sub | awk -F '@' '{print $1}')"
            if test "$lang" != und
                set cmd $cmd"-map $count "
                set cmd $cmd"-metadata:s:s:$(math $count - 1) "
                set cmd $cmd"language=$lang "
            end
        end
    else if ! set -q _flag_S
        set cmd $cmd'-map "0:s?" '
        for tag in (ffprobe -v quiet -show_entries stream=index,codec_type:stream_tags=language \
                        -select_streams s -of compact=p=0:nk=1 $_flag_i | grep -E '^[0-9]*\|.*\|.*')
            set lang "$(echo $tag | awk -F '|' '{print $3}')"
            if test "$lang" != und
                set cmd $cmd"-metadata:s:$(echo $tag | awk -F '|' '{print $1}') "
                set cmd $cmd"language=$lang "
            end
        end
    end

    set cmd $cmd'-map_metadata -1 '
    if set -q _flag_C
        set cmd $cmd'-map_chapters -1 '
    end

    if set -q _flag_H
        set cmd $cmd"-vf 'scale=w=-2:h=$_flag_H' "
    end

    if begin
            set -q _flag_e
            or set -q _flag_E
        end
        set cvinput (ffprobe -v quiet -select_streams v:0 -show_entries stream=codec_name \
                     -of default=nokey=1:noprint_wrappers=1 $_flag_i)
        set cainput (ffprobe -v quiet -select_streams a:0 -show_entries stream=codec_name \
                     -of default=nokey=1:noprint_wrappers=1 $_flag_i)

        # video encoding
        if begin
                test "$cvaccept[1]" = "$cvinput" -o "$cvaccept[2]" = "$cvinput"
                and ! set -q _flag_E
            end
            echo "Video already encoded with target codec, copying."
            set cmd $cmd'-c:v copy '
        else
            set cmd $cmd"-c:v $cv $cvflags"
        end

        # audio encoding
        if begin
                test "$caaccept[1]" = "$cainput" -o "$caaccept[2]" = "$cainput"
                and ! set -q _flag_E
            end
            echo "Audio already encoded with target codec, copying."
            set cmd $cmd'-c:a copy '
        else
            set cmd $cmd"-c:a $ca $caflags"
        end

        # subtitle encoding
        if ! set -q _flag_S
            set cmd $cmd"-c:s $cs $csflags"
        end
    else
        set cmd $cmd'-c:v copy -c:a copy '
        if ! set -q _flag_S
            set cmd $cmd'-c:s copy '
        end
    end

    set cmd $cmd"-y '$output'"

    echo $cmd\n
    echo "original_size=$(du --apparent-size -BK $_flag_i | awk -F 'K' '{print $1}')kB" \
        "original_time=$(ffprobe -v quiet -show_entries format=duration -sexagesimal -of default=noprint_wrappers=1:nokey=1 $_flag_i)" \
        "original_bitrate=$(math ( math (du --apparent-size -BK $_flag_i | awk -F 'K' '{print $1}') / (ffprobe -v quiet -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 $_flag_i | cut -d. -f1) x 8) | cut -d. -f1)kbits/s"
    echo
    if ! set -q _flag_D
        eval $cmd
    end

    if test $status -ne 0
        rm "$output"
        return 1
    else if set -q _flag_r
        rm "$_flag_i"
        if ! set -q _flag_n
            mv -f "$output" "$(echo "$output" | cut -c2-)"
        end
    end
end
