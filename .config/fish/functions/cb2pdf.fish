#!/usr/bin/env fish

function cb2pdf
    argparse 'i/input=' 'o/output=' -- $argv

    if begin
            set -q _flag_i
        end
    else
        echo "Input file needs to be specified."
        return 1
    end

    set -l dir "temp$(date +%s)"
    set -l file (echo "$_flag_i" | awk -F '.' '{ for (i=1; i<NF; ++i) { printf "."$i } }' | cut -c2-)
    set -l ext (echo "$_flag_i" | awk -F '.' '{ printf $NF }')

    mkdir -p $dir

    switch $ext
        case cbz
            unzip -q -d "$dir" "$_flag_i"
        case cbr
            unrar e "$_flag_i" "$dir"
        case '*'
            rm -rf $dir
            return 1
    end

    set out "$file.pdf"
    if set -q _flag_o
        set out "$_flag_o.pdf"
    end

    magick "$dir/*" "$out"

    if test -f index.info
        gs -sDEVICE=pdfwrite -q -dBATCH -dNOPAUSE \
            -sOutputFile="$out" index.info -f _"$out"
        mv _"$out" "$out"
    end

    rm -rf $dir
end
