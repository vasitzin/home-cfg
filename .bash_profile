[ -f ~/.bash_exports ] && . ~/.bash_exports
[ -f ~/.bash_aliases ] && . ~/.bash_aliases

# When logging in from tty1
# and no plasmashell instance exists
# start plasma
if [ "$(tty)" = "/dev/tty1" ] && \
   [[ $(pgrep -c "plasmashell") -lt 1 ]]; then
  startplasma-wayland
fi
