#!/bin/env bash

nproc=$(pgrep -c steam)
nrgid=$(echo $@ | grep -c rungameid)
if [ $nproc -lt 1 ] && [ $nrgid -gt 0 ]; then
  steam $@ 2>&1 | while read line; do
    if [[ $line == *"reaping pid"* ]]; then
      steam -shutdown
      break
    fi
  done
else
  steam $@
fi
