#!/usr/bin/env bash

s=1.25
killus=false

while getopts 'm:k' opt; do
  case "$opt" in
    m) s=$OPTARG;;
    k) killus=true;;
  esac
done; shift "$(($OPTIND - 1))"

[ $killus ] && \
  kill $(pgrep -fx "play -t wav -b 16 -e signed -c 1 -r 22050 -") && \
  exit 0

wl-paste -p | fmt -w 110

text=$(wl-paste -p | \
  sed -r 's/([A-Z]{1})([A-Z]{1})/\1 \2/g' | \
  sed -r 's/([a-zA-Z0-9]*)-([a-zA-Z0-9]*)/\1 \2/g' | \
  sed -r 's/ -* /, /g' | \
  sed -r 's/ –* /, /g' | \
  sed 's/\[.{1}\]//g' | \
  sed 's/\(.{1}\)//g' | \
  sed 's/\<.{1}\>//g')

echo "$text" | \
  mimic --length-scale $s 2>/dev/null | \
  play -t wav -b 16 -e signed -c 1 -r 22050 -
