#!/usr/bin/env bash

file=
quality=0

while getopts 'f:q:h' opt; do
  case "$opt" in
    f) file="$OPTARG";;
    q) quality="$OPTARG";;
    h)
      echo "-f <string> djvu file convert"
      echo "-q <string> result pdf quality"
      exit 0
      ;;
  esac
done; shift "$(($OPTIND - 1))"

[ ! -f "$file" ] && exit 99
file -i "$file" | grep image/vnd.djvu || exit 98

newtitle=$(echo $file | sed -r 's/(.*)\.djvu/\1\.pdf/')
ddjvu -format=pdf -quality=$quality "$file" "$newtitle"
