#!/usr/bin/env bash

file=

while getopts 'f:h' opt; do
  case "$opt" in
    f) file="$OPTARG";;
    h)
      echo "-f <string> pdf file to clear its metadata"
      exit 0
      ;;
  esac
done; shift "$(($OPTIND - 1))"

[ ! -f "$file" ] && exit 99
file -i "$file" | grep application/pdf || exit 98

exiftool -Author= -Title= -Description= -Subject= -CreateDate= -ModifyDate= \
-Creator= -Producer= -Keywords= -Trapped= "$file" && rm -rf "$file"_original
