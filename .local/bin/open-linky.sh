#!/usr/bin/env bash

if [[ "$1" == "linky://onion/"* ]]; then
    wid="$(echo "${1#linky://onion/}" | sed 's/ /%20/g')"
    notify-send -u NORMAL "Clipboard set" "$wid"
    wl-copy "$wid"
    flatpak ps --columns=app | rg torbrowser || flatpak run org.torproject.torbrowser-launcher
elif [[ "$1" == "linky://yt/"* ]]; then
    wid="${1#linky://yt/}"
    notify-send -u NORMAL "Loading your YT Video" "$wid"
    mpv --no-loop --ytdl-format="bestvideo[height<=?720]+bestaudio/best" "https://www.youtube.com/watch?v=$wid"
elif [[ "$1" == "linky://tak/"* ]]; then
    wid="${1#linky://tak/}"
    notify-send -u NORMAL "Loading your TikTok Video" "$wid"
    mpv --no-loop --ytdl-format="bestvideo[height<=?720]+bestaudio/best" "https://www.tiktok.com/$wid"
elif [[ "$1" == "linky://x/"* ]]; then
    wid="${1#linky://x/}"
    notify-send -u NORMAL "Loading your X Video" "$wid"
    mpv --no-loop --ytdl-format="bestvideo[height<=?720]+bestaudio/best" "https://www.x.com/$wid"
else
    xdg-open "$1" # Just open with the default handler
fi
