#!/usr/bin/env bash

vm=

while getopts 'v:' opt; do
  case "$opt" in
    v) vm="$OPTARG";;
  esac
done; shift "$(($OPTIND - 1))"

[[ -z "$vm" ]] && exit 99

if [ "$(systemctl is-active libvirtd)" = inactive ]; then
  systemctl start libvirtd
  if [ $? -ne 0 ]; then
    notify-send -a systemctl -i virt-manager 'libvirtd failed to start.'
    exit 1
  fi
fi

err="$(virsh --connect qemu:///system start "$vm" 2>&1)"
if [ $? -ne 0 ]; then
  notify-send -a virsh -i virt-manager "$vm vm failed to start." "$err"
  exit 1
fi

virt-viewer --connect qemu:///system --attach -w "$vm"
if [ $? -ne 0 ]; then
  notify-send -a virt-viewer -i virt-manager "virt-viewer failed to attach to $vm vm."
  exit 1
fi
