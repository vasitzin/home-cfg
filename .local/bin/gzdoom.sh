#!/usr/bin/env bash

titles=('The Ultimate DOOM' \
  SIGIL \
  'SIGIL II' \
  'DOOM II Hell on Earth' \
  'DOOM II No Rest for the Living' \
  'DOOM II Legacy of Rust' \
  'DOOM II Master Levels' \
  'Final DOOM The Plutonia Experiment' \
  'Final DOOM TNT Evilution' \
  Strife \
  HERETIC \
  HEXEN \
  'HEXEN Deathkings of the Dark Citadel')
wads=('-iwad doom.wad' \
  '-iwad doom.wad -file sigil.wad' \
  '-iwad doom.wad -file sigil2.wad' \
  '-iwad doom2.wad' \
  '-iwad doom2.wad -file nerve.wad' \
  '-iwad doom2.wad -file lor.wad' \
  '-iwad doom2.wad -file masterlevels.wad' \
  '-iwad plutonia.wad' \
  '-iwad tnt.wad' \
  '-iwad strife.wad' \
  '-iwad heretic.wad' \
  '-iwad hexen.wad' \
  '-iwad hexdd.wad -file hexdd-sndfix.wad')

cfg="-config .config/gzdoom/classic.ini"
dialog=
flag=zen
choice=

while getopts 'zk' opt; do
  case "$opt" in
    z) flag=zen;;
    k) flag=kde;;
  esac
done; shift "$(($OPTIND - 1))"

echo =================
echo = GZDOOM SELECT =
echo =================

case "$flag" in
  zen)
    if [[ -f /usr/bin/zenity ]]; then
      dialog="zenity --list --radiolist --width 512 --height 768"
      dialog="$dialog --title \"GZDoom Menu\" --column \"Select\" --column \"Game\" TRUE \"${titles[0]}\""
      for title in "${titles[@]:1}"; do
        dialog="$dialog FALSE \"$title\""
      done
      dialog="$dialog 2>/dev/null"
      echo "$: $dialog"
      choice="$(eval $dialog)"
      [[ "$choice" == "" ]] && echo "Aborting..." && echo ================= && exit 0
      for i in "${!titles[@]}"; do
        if [[ "${titles[$i]}" = "${choice}" ]]; then
          choice="${wads[$i]}"
        fi
      done
    else
      exit 97
    fi
    ;;
  kde)
    if [[ -f /usr/bin/kdialog ]]; then
      dialog="kdialog --geometry 384x512 --radiolist"
      dialog="$dialog \"GZDoom Menu\" -- \"${wads[0]}\" \"${titles[0]}\" on"
      for i in $(seq 1 $(( ${#titles[@]} - 1 ))); do
        dialog="$dialog \"${wads[$i]}\" \"${titles[$i]}\" off"
      done
      dialog="$dialog 2>/dev/null"
      echo "$: $dialog"
      choice="$(eval $dialog)"
      [[ "$choice" == "" ]] && echo "Aborting..." && echo ================= && exit 0
    else
      exit 97
    fi
    ;;
esac

#cmd="gzdoom $cfg $choice"
cmd="flatpak run org.zdoom.GZDoom -- $cfg $choice"
echo -----------------
echo "$: $cmd"
echo =================
echo
eval $cmd
