#!/usr/bin/env bash

loginctl terminate-session "$(loginctl list-sessions | grep "$(echo $(id -u))" | awk '{print $1}')"
