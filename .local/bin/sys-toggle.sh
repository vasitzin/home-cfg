#!/usr/bin/env bash

snd_dir=/usr/share/sounds/ocean/stereo
snd_start=outcome-success.oga
snd_stop=outcome-failure.oga
snd_error=dialog-error-critical.oga
is_user=0
service_name=

while getopts 'us:' opt; do
  case "$opt" in
    u) is_user=1;;
    s) service_name="$OPTARG";;
  esac
done; shift "$(($OPTIND - 1))"

cmd="systemctl"
[[ is_user -eq 1 ]] && cmd="$cmd --user"

if [[ -z "$service_name" ]]; then
  service_name="$(zenity --entry --title "System Service")"
  [[ $? -ne 0 ]] && exit 99
fi

if [[ "$($cmd is-active $service_name)" == inactive ]]; then
  if $cmd start $service_name; then
    [[ -d "$snd_dir" ]] && play $snd_dir/$snd_start
  else
    notify-send -a systemctl "$service_name failed to start."
    [[ -d "$snd_dir" ]] && play $snd_dir/$snd_error
    exit 98
  fi
else
  if $cmd stop $service_name; then
    [[ -d "$snd_dir" ]] && play $snd_dir/$snd_stop
  else
    notify-send -a systemctl "$service_name failed to stop."
    [[ -d "$snd_dir" ]] && play $snd_dir/$snd_error
    exit 98
  fi
fi
