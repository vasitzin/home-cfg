#!/usr/bin/env bash

user="$1"
ddir="$2"
timestamp=$(date +%s)

# CHECK USER
users=( $(awk -F ':' '{ print $1 }' /etc/passwd) )
[[ ! ${users[@]} =~ $user ]] && exit 99
unset users

# HANDLE PROCESSES
while [ $(pgrep -fc "bash .*books.sh.*") -gt 1 ];do
    list=( $(pgrep -f "bash .*books.sh.*") )
    [ ${list[0]} = $$ ] && break || sleep 1
done
unset list

gohandleln -u "$user" -d "$ddir" -t
