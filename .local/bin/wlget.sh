#!/bin/env bash

dlmgr="kget"
headrgx='^[cC]ontent-[tT]ype:.*(application|image|audio|video)/'
awkfmt='{print $2" | dl: "$3" | et: "$4}'
url="$(wl-paste)"

function ntfy {
  notify-send --replace-id $1 -e -u "$2" -i download-symbolic -a "wget $1" "$3" "$4"
}

if [ -f "/usr/bin/$dlmgr" ]; then
  ntfy $nid normal 'Downloading with KGet...' "$url"
  eval "$dlmgr \"$url\""
else
  nid=$(notify-send --print-id -e -u critical -i download-symbolic -a 'wget' 'Starting' '...')
  if [ $(curl -s -I -L "$url" | grep -E "$headrgx" | wc -l) -lt 1 ]; then
    ntfy $nid normal 'Failed' 'Probably not downloadable!'
  else
    cd ~/Downloads
    wget "$url" --show-progress --progress=dot -q 2>&1 | \
    while read line; do
      str=$(echo $line | sed 's/\.//g' | awk "$awkfmt")
      [ $(echo "$str" | grep "100%" | wc -l) -eq 1 ] && break
      ntfy $nid critical 'Downloading...' "$str"
      sleep 0.5
    done
    ntfy $nid normal 'Downloaded!' "$url"
  fi
fi
